﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using System.Linq;

namespace DataExport
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                string cnstr = ConfigurationManager.ConnectionStrings["MTLVECMSQL3"].ConnectionString;
                string outputfolder = ConfigurationManager.AppSettings["OutputFolder"];
                string outputFilename = "XCP_Client_Table_" + DateTime.Now.ToShortDateString().Replace("/", "") + ".csv";
                SqlConnection cn = new SqlConnection();
                SqlCommand cmd = new SqlCommand();
                cn.ConnectionString = cnstr;
                cn.Open();

                cmd.Connection = cn;
                cmd.CommandText = "APP.ClientSelectAll";
                cmd.CommandType = CommandType.StoredProcedure;

                StreamWriter sw = new StreamWriter(outputfolder + "\\" + outputFilename);
                SqlDataReader sr = cmd.ExecuteReader();

                DataTable Tablecolumns = new DataTable();

                for (int i = 0; i < sr.FieldCount; i++)
                {
                    Tablecolumns.Columns.Add(sr.GetName(i));
                }



                sw.WriteLine("\"" + string.Join("\",\"", Tablecolumns.Columns.Cast<DataColumn>().Select(csvfile => csvfile.ColumnName)) + "\"");
                Object[] values = new Object[sr.FieldCount];

                while (sr.Read())
                {
                    int fieldCount = sr.GetValues(values);
                    string datastr = "\"" + string.Join("\",\"", values) + "\"";

                    sw.WriteLine(datastr);
                }

                sw.Close();
                sw.Dispose();

                cn.Close();
                cn.Dispose();
                cmd.Dispose();

            }
            catch (Exception e)
            {
                string errmessage = e.Message;
            }

        }
    }
}
